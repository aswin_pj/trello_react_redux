import React from "react";
import { FaHome, FaSearch, FaBars } from "react-icons/fa";
// import image from ".";

class Header extends React.Component {
  // componentWillMount() {
  //   console.log('hhhhhhh')
  //   fetch('https://www.youtube.com/watch?v=KWodksuroHs')
  //   .then(res => res.json())
  //   .then(r => console.log(r))
  // }
  render() {
    const logo = require("./trello-logo-blue.png");

    return (
      <div className="header">
        <div className="header-left-pannel">
          {<FaHome />}
          <div className="header-board-title">Boards</div>
          {<FaSearch />}
        </div>
        <img src={logo} alt="logo" />
        <div className="header-right-pannel">{<FaBars />}</div>
      </div>
    );
  }
}
export default Header;

import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./App.css";

class BoardCard extends Component {
  render() {
    var image = this.props.data.prefs.backgroundImage;
    if (image === null) {
      image = this.props.data.prefs.backgroundColor;
      return (
        <div
          className="board-card"
          style={{ backgroundColor: this.props.data.prefs.backgroundColor }}
          key={this.props.data.id}
        >
          <Link
            to={{
              pathname: `/boards/${this.props.data.id}`
            }}
          >
            <h1>{this.props.data.name}</h1>
          </Link>
        </div>
      );
    }
    return (
      <Link to={`/boards/${this.props.data.id}`} key={this.props.data.id}>
        <div className="board-card" key={this.props.data.id}>
          <img src={image} alt="board" />
          <h1>{this.props.data.name}</h1>
        </div>
      </Link>
    );
  }
}

export default BoardCard;

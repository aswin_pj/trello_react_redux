import React from "react";
// import { apiKEY, tokenKEY } from "./url";
import "./App.css";
import Card from "./cards";
import { connect } from "react-redux";
import { fetchCards, postCard, deleteCard } from "./actions/fetchaction";

class Lists extends React.Component {
  state = {
    input: ""
  };

  componentDidMount() {
    this.props.fetchCards(this.props.listdata.id);
  }


  render() {
    var carData = [];
    carData = Object.entries(this.props.cards).filter(c => {
    
      var keys = Object.keys(c[1]);
      var values = Object.values(c[1]);

      if (keys[0] === `card-${this.props.listdata.id}`) {
        // console.log("values = ", values)
        return values[0]
      }
    });
    // let data = carData.slice(0, carData.length)
    // console.log('cardData = ', data)
    let data = carData.map(elem => Object.values(elem[1])[0])
     console.log('cardData1 = ', data)
    return (
      <div className="board">
        <h3>{this.props.listdata.name}</h3>
        <div id="all-cards">
          {data.map(c => (
            <Card
              key={c.id}
              name={c.name}
              data={c}
              onDelete={this.deleteCard}
            />
          ))}
        </div>
        <div className="add-card">
          <textarea
            name="card-title"
            id="card-title"
            cols="22"
            rows="3"
            placeholder="Enter a title For this Card"
            onChange={this.handleChange}
            value={this.state.input}
          ></textarea>
          <i className="fas fa-pencil-alt prefix"></i>
          <div id="buttons">
            <button id="add-card" onClick={this.addCard}>
              Add Card
            </button>
            <button id="close">X</button>
          </div>
        </div>
      </div>
    );
  }

  handleChange = event => {
    this.setState({
      input: event.target.value
    });
  };

  addCard = () => {
    this.props.postCard(this.props.listdata.id, this.state.input);
  };

  deleteCard = (id, idList) => {
    console.log("id  to be delete ", id)
    console.log("id List to be delete ", this.props.listdata.id)
    const listId = this.props.listdata.id
    this.props.deleteCard(id, listId);
  };
 
}
const mapStateToProps = state => ({
  cards: state.boards.cardsData
});

export default connect(
  mapStateToProps, 
  { fetchCards, postCard, deleteCard }
)(Lists);

import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import Board from "./board";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { createStore, applyMiddleware, compose } from "redux";
import Header from "./header";
import "./App.css";
import { Provider } from "react-redux";
import * as serviceWorker from "./serviceWorker";
import TotalLists from "./totallists";
import rootReducer from "./reducers";

import thunk from "redux-thunk";

const store = createStore(
  rootReducer, 
  compose(
    applyMiddleware(thunk),
  )
)

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <React.Fragment>
        <Header />
        <div className="routes-path">
          <Switch>
            <Route exact path="/" component={Board} />
            <Route path="/boards/:id" component={TotalLists} />
          </Switch>
        </div>
      </React.Fragment>
    </Router>
  </Provider>,
  document.getElementById("root")
);
// ReactDOM.render(<Board />, document.getElementById("root"));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
//
serviceWorker.unregister();

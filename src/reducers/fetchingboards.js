
const initialSatate = {
  data: [],
  listData: [],
  cardsData: [],
  list: {},
  cards: [],
  card: [],
  checklistData: []
};
export default function(state = initialSatate, action) {
  switch (action.type) {
    case "FETCH_BOARDS":
      return {
        ...state,
        data: action.boardsData
      };
    case "FETCH_LISTS":
      return {
        ...state,
        listData: action.listData
      };
    case "FETCH_CARDS":
      return {
        ...state,
        cardsData: state.cardsData.concat({
          [`card-${action.listId}`]: action.cardsData
        })
      };
    case "FETCH_CHECKLIST":
      return {
        ...state,
        checklistData: action.checklistData
      };
    case "POST_LISTS":
      return {
        ...state,
        listData: state.listData.concat(action.listData)
      };
    case "POST_CARD":
      return {
        ...state,
        cardsData: state.cardsData.concat({
          [`card-${action.listId}`]: action.card
        })
      };
    case "POST_CHECKLIST":
      return {
        ...state
      };
    case "DELETE_CHECKLIST":
      return {
        ...state
      };
    case "DELETE_CARD":
      return {
        ...state,
        cardsData: state.cardsData.filter(elem => elem[`card-${action.listId}`].id !== action.cardId )        
      }
    default:
      return state;
  }
}
